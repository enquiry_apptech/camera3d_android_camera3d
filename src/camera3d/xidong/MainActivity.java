package camera3d.xidong;

import camera3d.xidong.R;
import camera3d.xidong.controller.FeedListAdapter;
import camera3d.xidong.controller.AppController;
import camera3d.xidong.model.FeedItem;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Cache.Entry;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class MainActivity extends Activity {
	private static final String TAG = MainActivity.class.getSimpleName();
	private ListView listView;
	private FeedListAdapter listAdapter;
	private List<FeedItem> feedItems;
	private static final String URL_FEED = "http://54.251.251.94/camera3d/www/index.php/xidong/resource/get_simple_feeds";
	private ActionBar actionBar;
	private AdView mAdView ;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mAdView = new AdView(this, AdSize.SMART_BANNER, "ca-app-pub-8709149684611651/7761232126");
		LinearLayout layout = (LinearLayout)findViewById(R.id.google_adv);
		layout.addView(mAdView);
		
		AdRequest adRequest = new AdRequest();
		
		mAdView.loadAd(adRequest);
		
		listView = (ListView) findViewById(R.id.list);

		feedItems = new ArrayList<FeedItem>();

		listAdapter = new FeedListAdapter(this, feedItems);
		listView.setAdapter(listAdapter);
		
		actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.action_bar);
		
		Cache cache = AppController.getInstance().getRequestQueue().getCache();
		Entry entry = cache.get(URL_FEED);
		if (entry != null) {
			try {
				String data = new String(entry.data, "UTF-8");
				try {
					parseJsonFeed(new JSONObject(data));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		} else {
			Response.Listener<JSONObject> response = null;
			Response.ErrorListener response_error = null;
			
			Map<String, String> jsonParams = new HashMap<String, String>();
			jsonParams.put("item_uid", "1001");

			StringRequest jsonReq = new StringRequest(Method.POST,
					URL_FEED, new Response.Listener<String>(){

						@Override
						public void onResponse(String response) {							
							try {
								JSONObject jObject = new JSONObject(response);
								parseJsonFeed(jObject);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							// TODO Auto-generated method stub						
						}
				    },
		            response_error = new Response.ErrorListener() {
				    	public void onErrorResponse(VolleyError error) {
							VolleyLog.d(TAG, "Error: " + error.getMessage());
						}
				    }) {
				@Override
		        public Map<String, String> getHeaders() throws AuthFailureError {
		            HashMap<String, String> headers = new HashMap<String, String>();
		            headers.put("Content-Type", "application/x-www-form-urlencoded");
		            return headers;
		        }
				
				@Override
				protected Map<String, String> getParams() {
					Map<String, String> params = new HashMap<String, String>();
					params.put("item_uid", "1001");
					return params;
				}
			};
			AppController.getInstance().addToRequestQueue(jsonReq);
		}		
	}
	
	private void parseJsonFeed(JSONObject response) {
		try {
			JSONArray feedArray = response.getJSONArray("item");

			for (int i = 0; i < feedArray.length(); i++) {
				JSONObject feedObj = (JSONObject) feedArray.get(i);

				FeedItem item = new FeedItem();
				item.setId(feedObj.getInt("id"));
				item.setName(feedObj.getString("name"));

				String image = feedObj.isNull("image") ? null : feedObj
						.getString("image");
				item.setImge(image);
				item.setStatus(feedObj.getString("status"));
				item.setProfilePic(feedObj.getString("profilePic"));
				String a = "1403375851930";
				item.setTimeStamp(a);
				item.setLike(feedObj.getInt("like"));

				String feedUrl = feedObj.isNull("url") ? null : feedObj
						.getString("url");
				item.setUrl(feedUrl);

				feedItems.add(item);
			}
			
			listAdapter.notifyDataSetChanged();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onDestroy() {
		  mAdView.destroy();
		  super.onDestroy();
		}

	
}
